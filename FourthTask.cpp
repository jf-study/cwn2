//
// Created by Jamefrus on 11.12.2015.
//

#include <stdio.h>
#include "tasks.h"
#include "util.h"

struct Matrix {
    float **array;
    int rows;
    int lines;


    Matrix(int rows, int lines) : rows(rows), lines(lines) { }

    Matrix(float **array, int rows, int lines) : array(array), rows(rows), lines(lines) { }
};



Matrix readMatrix() {
    int lines = 0;
    PRINTLN("Size of array's lines: ");
    scanf("%d", &lines);
    int rows = 0;
    PRINTLN("Size of array's rows: ");
    scanf("%d", &rows);
    Matrix arr = Matrix(rows, lines);
    for (int i = 0; i < lines; ++i) {
        printf("Input content of %d line: \n",i+1);
        arr.array[i] = scanReals(rows);
    }
    return arr;
}

void processMatrix(const Matrix &arr) {
    for (int i = 0; i < arr.lines; ++i) {
        float *subarr = arr.array[i];
        float max = subarr[0];
        int maxIndex = 0;
        float middle = 0;
        for (int j = 1; j < arr.rows; ++j) {
            float value = subarr[j];
            float absValue = value < 0?-value:value;
            if(absValue > max) {
                middle += max;
                max = absValue;
                maxIndex = j;
            } else middle += value;
        }
        middle /= arr.rows;
        subarr[maxIndex] = middle;
    }
}

void printMatrix(Matrix matrix) {
    for (int i = 0; i < matrix.lines; ++i) {
        if(i!=0) PRINTLN("");
        printReals(matrix.rows,matrix.array[i],"%f");
    }
}

void task4(){
    Matrix arr = readMatrix();
    processMatrix(arr);
    printMatrix(arr);
}