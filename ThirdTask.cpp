//
// Created by Jamefrus on 11.12.2015.
//

#include <stdio.h>
#include "tasks.h"
#include "util.h"

#define UNEVEN obj & 1 == 1
#define ALL_EVEN -1

struct SizedArr {
    int* array;
    int length;
    SizedArr(int *array, int length) : array(array), length(length) { }
};

int getUnevenIndex(SizedArr arr){
    for (int i = arr.length; i >= 0; --i) {
        int obj = arr.array[i];
        if(UNEVEN){
            return i;
        }
    }
    return ALL_EVEN;
}

void align(SizedArr arr, int deletedIndex){
    if(deletedIndex == arr.length) return;
    for (int i = deletedIndex; i < arr.length; ++i) {
        arr.array[i] = arr.array[i+1];
    }
}

SizedArr readArray() {
    int count = 0;
    PRINTLN("Size of array: ");
    scanf("%d", &count);
    PRINTLN("Input content of array: ");
    return SizedArr(scanDecimals(count), count);
}

void process(SizedArr &arr) {
    int toDeleteIndex = getUnevenIndex(arr);
    if(toDeleteIndex == ALL_EVEN) throw ALL_EVEN;
    arr.length--;
    align(arr,toDeleteIndex);
}

void print(SizedArr arr) {
    PRINTLN("Output array: ");
    printDecimals(arr.length, arr.array,"%d");
}

void task3(){
    SizedArr arr = readArray();
    process(arr);
    print(arr);
}


