//
// Created by Jamefrus on 11.12.2015.
//

#ifndef UNTITLED_UTIL_H
#define UNTITLED_UTIL_H

float * scanReals(int count);
int * scanDecimals(int count);


void printReals(int length, const float *array, const char *format);
void printDecimals(int length, const int *array, const char *format);
#endif //UNTITLED_UTIL_H
