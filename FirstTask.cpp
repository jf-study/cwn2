//
// Created by Jamefrus on 11.12.2015.
//

#include <stdio.h>

const float u1 = 3.15;
const float u2 = 18.88;
const float u3 = 8.35;

float consume(float x1, float x2, float x3) {
    return u1 * x1 + u2 * x2 + u3 * x3;
}

 void task1(){
    float x1 = 0;
    float x2 = 0;
    float x3 = 0;
    printf("%s", "Please, input any three numbers (maybe float): ");
    scanf("%f %f %f",&x1, &x2, & x3);
    printf("%f", consume(x1, x2, x3));
}


