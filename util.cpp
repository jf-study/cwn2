//
// Created by Jamefrus on 11.12.2015.
//

#include <stdio.h>
#include "tasks.h"
#include "util.h"

float *scanReals(int count) {
    float *array = new float[count];
    for (int i = 0; i < count; ++i) {
        scanf("%f",&array[i]);
    }
    return array;
}


int *scanDecimals(int count) {
    int *array = new int[count];
    for (int i = 0; i < count; ++i) {
        scanf("%d",&array[i]);
    }
    return array;
}


void printReals(int length, const float *array, const char *format) {
    for (int i = 0; i < length; ++i) {
        float element = array[i];
        printf(format,element);
    }
}



void printDecimals(int length, const int *array, const char *format) {
    for (int i = 0; i < length; ++i) {
        int element = array[i];
        printf(format,element);
    }
}