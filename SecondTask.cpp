//
// Created by Jamefrus on 11.12.2015.
//
#include <stdio.h>
#include <iostream>

#define f1 0,-6,8,-42
#define f2 -3,-5,7,-33
#define f3 10,3,2,57
#define VALID(eq) valid(i,j,k,eq)
#define leftPart x1 * u1 + x2 * u2 + x3 * u3

using namespace std;

bool valid(int x1, int x2, int x3, int u1, int u2, int u3, int answer) {
    return leftPart == answer;
}

bool check(int i, int j, int k) {
    return VALID(f1) && VALID(f2) && VALID(f3);
}

bool apply() {
    for (int i = 0; i < 21; ++i) {
        for (int j = 0; j < 21; ++j) {
            for (int k = 0; k < 21; ++k) {
                if (check(i, j, k)) {
                    printf("%d %d %d", i,j,k);
                    return true;
                }
            }
        }
    }
    return false;
}

 void  task2() {
    if (!apply()) printf("%s", "Ups...");
}

